/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.topics;

import com.google.common.collect.Multimap;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AnalyseTopicDistribution {

	private static final Pattern PATTERN_TOP_TERMS =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*[^sliced]-top-terms.csv)" );

	private static final String ROOT_FILE = "/mnt/Damocles/Master";

	public static void main ( final String[] args ) throws IOException {

		File ROOT = new File ( ROOT_FILE );
		Set<File> directories = new HashSet<> ();
		getAllDirectories ( ROOT, directories, 2 );

		for ( File file : directories ) {
			extractTopTerms ( file );
		}


	}

	private static void extractTopTerms ( final File directory ) throws IOException {
		File[] files = directory.listFiles ();
		for ( File file : files ) {
			Matcher matcher = PATTERN_TOP_TERMS.matcher ( file.getAbsolutePath () );
			if ( matcher.find () ) {
				Multimap<String, String> topics = PredictionUtils.readWordsFromTopics ( file.getAbsolutePath () );
				Map<String, Integer> numbering = new HashMap<> ();
				Map<Integer, Integer> list = new TreeMap<> ();
				for ( String word : topics.values () ) {
					if ( numbering.containsKey ( word ) ) {
						numbering.put ( word, numbering.get ( word ) + 1 );
					} else {
						numbering.put ( word, 1 );
					}
				}
				for ( Map.Entry<String, Integer> entry : numbering.entrySet () ) {
					if ( list.containsKey ( entry.getValue () ) ) {
						list.put ( entry.getValue (), list.get ( entry.getValue () ) + 1 );
					} else {
						list.put ( entry.getValue (), 1 );
					}
				}
				File f = new File ( file.getAbsolutePath () + ".analysis.csv" );
				f.createNewFile ();
				FileOutputStream fos = new FileOutputStream ( f );
				BufferedWriter writer = new BufferedWriter ( new OutputStreamWriter ( fos ) );
				for ( Map.Entry<Integer, Integer> entry : list.entrySet () ) {
					writer.write ( entry.getKey () + "\t" + entry.getValue () );
					writer.newLine ();
				}
				writer.flush ();
				writer.close ();
				fos.close ();
			}
		}
	}

	public static void getAllDirectories ( final File directory, final Set<File> directories, int limit ) {
		if ( directory.exists () && limit > 0 ) {
			File[] files = directory.listFiles ();
			if ( null != files ) {
				for ( int i = 0; i < files.length; i++ ) {
					if ( files[ i ].isDirectory () ) {
						getAllDirectories ( files[ i ], directories, limit - 1 );
					}
				}
			}
		}
		directories.add ( directory );
	}
}
