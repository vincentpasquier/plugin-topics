/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.topics.extractor;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.RecommendationClient;
import ch.hesso.predict.plugins.topics.PredictionUtils;
import ch.hesso.predict.restful.PublicationAbstract;
import ch.hesso.predict.restful.Recommendation;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.base.Joiner;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.GaussianProcesses;
import weka.classifiers.timeseries.WekaForecaster;
import weka.classifiers.timeseries.core.TSLagMaker;
import weka.core.Instance;
import weka.core.Instances;

import java.io.*;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TopicModelProcessor {

	private final long startYear;

	private final long endYear;

	private final long topicNumber;

	private final String[] keywords;

	private final String[] acronyms;

	private final String[] titles;

	private static final String FILE_PREFIX =
			"topics_";

	private static final String FILE_SUFFIX =
			".csv";

	private static final String LDA_EXEC =
			TopicModelProcessor.class.getResource ( "/tmt-0.4.0.jar" ).getFile ();

	private static final String LDA_SCALA_PREFIX =
			"llda";

	private static final String LDA_SCALA_SUFFIX =
			".scala";

	private static final String LDA_VAR_FILE_PATTERN =
			"\\$file\\$";

	private static final String LDA_VAR_TOPIC_PATTERN =
			"\\$paramTopicNumber\\$";

	private static final String LDA_SCALA_TEMPLATE =
			TopicModelProcessor.class.getResource ( "/" + LDA_SCALA_PREFIX + LDA_SCALA_SUFFIX ).getFile ();

	private static final Pattern LDA_PATTERN =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*)" );

	private static final Pattern PATTERN_DOC_TOPIC_DIS =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*-document-topic-distributions.csv)" );

	private static final Pattern PATTERN_PER_WORD_PER_TOPIC =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*-sliced-top-terms.csv)" );

	private static final Pattern PATTERN_USAGE =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*-usage.csv)" );

	private static final Pattern PATTERN_TOP_TERMS =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*-top-terms.csv)" );

	private static final Pattern PATTERN_SLICE_USAGE =
			Pattern.compile ( "(llda\\-[A-z0-9\\-]*/[A-z0-9\\-_]*-sliced-usage.csv)" );

	private final PluginInterface.PluginStatus.Builder builder =
			PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );

	private final Recommendation recommendation;

	private int queryHash;

	private String ldaDirectory;

	private String topTerms;

	private String sliceUsage;

	private Multimap<String, PredictionUtils.TopicUsageEntry> usage;

	private String scalaFileStr;

	public TopicModelProcessor ( final long startYear,
															 final long endYear,
															 final long topicNumber,
															 final String[] keywords,
															 final String[] acronyms,
															 final String[] titles ) {
		this.startYear = startYear;
		this.endYear = endYear;
		this.topicNumber = topicNumber;
		this.keywords = keywords;
		this.acronyms = acronyms;
		this.titles = titles;
		recommendation = new Recommendation ();
	}

	private static Map<String, Object> toReturn = new HashMap<> ();

	public void run () {

		/*if ( !toReturn.isEmpty () ) {
			builder.text ( "Successfully analyzed topic from given parameters." );
			builder.status ( AnnotatedPlugin.State.DONE );
			builder.objects ( toReturn );
			PluginInterface.publishPluginStatus ( builder.build () );
			return;
		}*/

		PluginInterface.publishPluginStatus ( builder.build () );
		recommendation.setStartYear ( (int) startYear );
		recommendation.setEndYear ( (int) endYear );
		recommendation.setKeywords ( new HashSet<> ( Arrays.asList ( keywords ) ) );
		recommendation.setConferenceAcronym ( new HashSet<> ( Arrays.asList ( acronyms ) ) );
		recommendation.setConferenceTitle ( new HashSet<> ( Arrays.asList ( titles ) ) );
		queryHash = recommendation.hashCode ();

		boolean success = true;

		builder.progress ( 1 ).totalProgress ( 3 );
		if ( !cachedQuery ( queryHash ) ) {
			builder.text ( "Gathering abstracts from RESTful API with given parameters<br>" + parametersMessage () );
			PluginInterface.publishPluginStatus ( builder.build () );
			success = query ( recommendation, queryHash );
		}

		if ( !success ) {
			builder.text ( "Unable to query server with recommendation parameters, check logs" );
			builder.status ( AnnotatedPlugin.State.ERROR );
			PluginInterface.publishPluginStatus ( builder.build () );
			cleanup ();
			return;
		}

		builder.progress ( 2 );
		success = launchLDA ( queryHash );
		if ( !success ) {
			builder.text ( "Unable to run labelled LDA with recommendation parameters, check logs" );
			builder.status ( AnnotatedPlugin.State.ERROR );
			PluginInterface.publishPluginStatus ( builder.build () );
			cleanup ();
			return;
		}

		success = forecast ();
		if ( !success ) {
			builder.text ( "Unable to run prediction on topics with recommendation parameters, check logs" );
			builder.status ( AnnotatedPlugin.State.ERROR );
			PluginInterface.publishPluginStatus ( builder.build () );
			cleanup ();
			return;
		}

		Set<PredictionUtils.TopicUsageEntries> predictions = new TreeSet<> ();
		for ( String topic : usage.keySet () ) {
			Collection<PredictionUtils.TopicUsageEntry> entries = usage.get ( topic );
			List<PredictionUtils.TopicUsageEntry> sorted = new ArrayList<> ( entries );
			Collections.sort ( sorted );
			PredictionUtils.TopicUsageEntries prediction = new PredictionUtils.TopicUsageEntries ( topic, sorted );
			predictions.add ( prediction );
		}

		List<Object> objects = new ArrayList<> ();
		Multimap<String, String> words = PredictionUtils.readWordsFromTopics ( topTerms );

		for ( PredictionUtils.TopicUsageEntries entries : predictions ) {
			Map<String, Object> topicEntry = new HashMap<> ();
			List<Double[]> yearsEntry = new ArrayList<> ();

			for ( PredictionUtils.TopicUsageEntry entry : entries.getEntries () ) {

				Double[] mappingYear = new Double[ 2 ];
				mappingYear[ 0 ] = (double) entry.getYear ();
				mappingYear[ 1 ] = entry.getDocuments ();
				yearsEntry.add ( mappingYear );
			}

			topicEntry.put ( "words", words.get ( entries.getTopic () ) );
			topicEntry.put ( "years", yearsEntry );
			topicEntry.put ( "topic", entries.getTopic () );

			objects.add ( topicEntry );
		}
		toReturn.put ( "result", objects );

		builder.text ( "Successfully analyzed topic from given parameters." );
		builder.status ( AnnotatedPlugin.State.DONE );
		builder.objects ( toReturn );
		PluginInterface.publishPluginStatus ( builder.build () );

		cleanup ();
	}

	private void cleanup () {
		File lldaDir = new File ( ldaDirectory );
		if ( lldaDir.isDirectory () ) {
			deleteDirectory ( lldaDir );
		}
		File scalaFile = new File ( scalaFileStr );
		if ( scalaFile.isFile () ) {
			scalaFile.delete ();
		}

	}

	public static boolean deleteDirectory ( File directory ) {
		if ( directory.exists () ) {
			File[] files = directory.listFiles ();
			if ( null != files ) {
				for ( int i = 0; i < files.length; i++ ) {
					if ( files[ i ].isDirectory () ) {
						deleteDirectory ( files[ i ] );
					} else {
						files[ i ].delete ();
					}
				}
			}
		}
		return ( directory.delete () );
	}

	private boolean forecast () {
		builder.text ( "Running Forecasting with given parameters<br>" + parametersMessage () );
		PluginInterface.publishPluginStatus ( builder.build () );
		boolean success = true;

		Map<String, Instances> instances = new HashMap<> ();
		usage = PredictionUtils.loadTopicData ( sliceUsage );

		for ( String topic : usage.keySet () ) {
			Collection<PredictionUtils.TopicUsageEntry> entries = usage.get ( topic );
			List<PredictionUtils.TopicUsageEntry> sorted = new ArrayList<> ( entries );
			Collections.sort ( sorted );
			try {
				instances.put ( topic, PredictionUtils.generateInstances ( sorted ) );
			} catch ( ParseException e ) {
			}
		}

		try {
			for ( String topic : instances.keySet () ) {
				WekaForecaster forecaster = new WekaForecaster ();
				forecaster.setFieldsToForecast ( "Documents,Word" );
				forecaster.setBaseForecaster ( new GaussianProcesses () );
				forecaster.getTSLagMaker ().setTimeStampField ( "Year" );
				forecaster.getTSLagMaker ().setPeriodicity ( TSLagMaker.Periodicity.YEARLY );
				Instances instance = instances.get ( topic );
				forecaster.buildForecaster ( instance, System.err );
				forecaster.primeForecaster ( instance );
				List<List<NumericPrediction>> forecast = forecaster.forecast ( 1, System.err );
				for ( int i = 0; i < 1; i++ ) {
					List<NumericPrediction> predsAtStep = forecast.get ( i );
					if ( predsAtStep.size () >= 2 ) {
						PredictionUtils.TopicUsageEntry entry = new PredictionUtils.TopicUsageEntry ();
						entry.setYear ( (int) ( endYear + i + 1 ) );
						entry.setTopic ( topic );
						Instance lastYear = instance.lastInstance ();
						double documents = predsAtStep.get ( 0 ).predicted ();
						/*double lastYearDocuments = lastYear.value ( new Attribute ( "Documents" ) );
						if ( Math.abs ( documents - lastYearDocuments ) > 50 ) {
							boolean trendUp = documents - lastYearDocuments > 0;
							if ( trendUp ) {
								documents = lastYearDocuments + 20;
							} else {
								documents = lastYearDocuments - 20;
							}
						}*/
						entry.setDocuments ( documents );
						entry.setWords ( predsAtStep.get ( 1 ).predicted () );
						usage.put ( topic, entry );
					}
				}
			}
		} catch ( Exception e ) {
		}

		return success;
	}

	private boolean launchLDA ( final int queryHash ) {
		builder.text ( "Running Topic modeling with given parameters<br>" + parametersMessage () );
		PluginInterface.publishPluginStatus ( builder.build () );
		boolean success = createScalaFile ( queryHash );
		scalaFileStr = LDA_SCALA_PREFIX + String.valueOf ( queryHash ) + LDA_SCALA_SUFFIX;
		File scalaFile = new File ( scalaFileStr );

		if ( !success ) {
			return success;
		}

		try {
			Process process =
					Runtime.getRuntime ().exec (
							new String[] { "java", "-jar", LDA_EXEC, scalaFile.getAbsolutePath () } );
			process.waitFor ();
			InputStream isIs = process.getInputStream ();
			byte[] in = new byte[ isIs.available () ];
			isIs.read ( in, 0, in.length );
			String output = new String ( in );
			Matcher matcher = PATTERN_SLICE_USAGE.matcher ( output );
			if ( matcher.find () ) {
				sliceUsage = matcher.group ( 1 );
			}
			matcher = PATTERN_TOP_TERMS.matcher ( output );
			if ( matcher.find () ) {
				topTerms = matcher.group ( 1 );
			}
			matcher = LDA_PATTERN.matcher ( output );
			if ( matcher.find () ) {
				ldaDirectory = matcher.group ( 1 );
			}
			Thread.sleep ( 2000 );
			success = true;
		} catch ( IOException | InterruptedException e ) {
			success = false;
		}

		return success;
	}

	private boolean createScalaFile ( final int queryHash ) {
		try {
			List<String> lines =
					Files.readLines ( new File ( LDA_SCALA_TEMPLATE ), Charset.defaultCharset () );
			String content = Joiner.on ( "\n" ).join ( lines );
			content = content.replaceAll ( LDA_VAR_FILE_PATTERN, FILE_PREFIX + String.valueOf ( queryHash ) + FILE_SUFFIX );
			content = content.replaceAll ( LDA_VAR_TOPIC_PATTERN, "" + topicNumber );
			FileOutputStream fos = new FileOutputStream (
					new File ( LDA_SCALA_PREFIX + String.valueOf ( queryHash ) + LDA_SCALA_SUFFIX ) );
			BufferedWriter writer = new BufferedWriter ( new OutputStreamWriter ( fos ) );
			writer.write ( content );
			writer.close ();
			fos.close ();
		} catch ( IOException e ) {
			return false;
		}
		return true;
	}

	private boolean query ( final Recommendation recommendation, final int queryHash ) {
		RecommendationClient client = RecommendationClient.create ( APIClient.REST_API );
		List<PublicationAbstract> abstracts = client.postRecommendation ( recommendation );
		try {
			FileOutputStream fos =
					new FileOutputStream ( FILE_PREFIX + String.valueOf ( queryHash ) + FILE_SUFFIX );
			BufferedWriter writer = new BufferedWriter ( new OutputStreamWriter ( fos ) );
			int i = 1;
			for ( PublicationAbstract abs : abstracts ) {
				String line = i + "," + abs.getYear () + ",\"" + abs.getText ().replaceAll ( "\"", "" ) + "\"";
				writer.write ( line );
				writer.newLine ();
				i++;
			}
			writer.flush ();
			writer.close ();
			fos.close ();
		} catch ( IOException e ) {
			return false;
		}
		return true;
	}

	private boolean cachedQuery ( final int hash ) {
		File f = new File ( FILE_PREFIX + String.valueOf ( hash ) + FILE_SUFFIX );
		return f.exists ();
	}

	private String parametersMessage () {
		StringBuilder sb =
				new StringBuilder ( "<ul>" );
		sb.append ( "<li>Year start: " ).append ( startYear ).append ( "</li>" );
		sb.append ( "<li>Year end: " ).append ( endYear ).append ( "</li>" );
		sb.append ( "<li>Topic number: " ).append ( topicNumber ).append ( "</li>" );
		if ( keywords.length > 0 ) {
			sb.append ( "<li>Keywords: " );
			for ( String keyword : keywords ) {
				sb.append ( keyword + " " );
			}
			sb.append ( "</li>" );
		}
		if ( acronyms.length > 0 ) {
			sb.append ( "<li>Acronyms: " );
			for ( String acronym : acronyms ) {
				sb.append ( acronym + " " );
			}
			sb.append ( "</li>" );
		}
		if ( titles.length > 0 ) {
			sb.append ( "<li>Titles: " );
			for ( String title : titles ) {
				sb.append ( title + " " );
			}
			sb.append ( "</li>" );
		}
		sb.append ( "</ul>" );
		return sb.toString ();
	}
}
