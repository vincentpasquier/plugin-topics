/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.topics;

import com.google.common.base.Splitter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PredictionUtils {


	public static Multimap<String, TopicUsageEntry> loadTopicData ( final String file ) {
		Multimap<String, TopicUsageEntry> loaded = HashMultimap.create ();

		try {
			List<String> lines = Files.readLines ( new File ( file ), Charset.defaultCharset () );
			for ( int i = 1; i < lines.size (); i++ ) {
				List<String> tokens = Splitter.on ( "," ).splitToList ( lines.get ( i ) );
				if ( tokens.size () >= 4 ) {
					TopicUsageEntry entry = new TopicUsageEntry ( tokens );
					loaded.put ( entry.getTopic (), entry );
				}
			}
		} catch ( IOException e ) {
		}

		return loaded;
	}

	public static Instances generateInstances ( final List<TopicUsageEntry> sorted ) throws ParseException {
		Attribute date = new Attribute ( "Year", "yyyy" );
		Attribute documents = new Attribute ( "Documents" );
		Attribute words = new Attribute ( "Word" );
		ArrayList<Attribute> attributeList = new ArrayList<> ();
		attributeList.add ( date );
		attributeList.add ( documents );
		attributeList.add ( words );
		Instances instances = new Instances ( "Topic", attributeList, 0 );

		for ( TopicUsageEntry entry : sorted ) {
			double[] instance = new double[ instances.numAttributes () ];
			instance[ 0 ] = instances.attribute ( 0 ).parseDate ( "" + entry.getYear () );
			instance[ 1 ] = entry.getDocuments ();
			instance[ 2 ] = entry.getWords ();
			instances.add ( new DenseInstance ( 1, instance ) );
		}

		return instances;
	}

	public static Multimap<String, String> readWordsFromTopics ( final String topTerms ) {
		Multimap<String, String> topWords = HashMultimap.create ();

		try {
			List<String> lines = Files.readLines ( new File ( topTerms ), Charset.defaultCharset () );
			for ( int i = 1; i < lines.size (); i++ ) {
				List<String> tokens = Splitter.on ( "," ).splitToList ( lines.get ( i ) );
				String topic = tokens.get ( 0 );
				List<String> top = tokens.subList ( 1, tokens.size () - 1 );
				topWords.putAll ( topic, top );
			}
		} catch ( IOException e ) {
			e.printStackTrace ();
		}

		return topWords;
	}

	public static final class TopicUsageEntry implements Comparable<TopicUsageEntry> {

		private String topic;

		private int year;

		private double documents;

		private double words;

		public TopicUsageEntry ( final List<String> entry ) {
			topic = entry.get ( 0 );
			year = Integer.parseInt ( entry.get ( 1 ) );
			documents = Double.parseDouble ( entry.get ( 2 ) );
			words = Double.parseDouble ( entry.get ( 3 ) );
		}

		public TopicUsageEntry () {
		}

		public String getTopic () {
			return topic;
		}

		public void setTopic ( final String topic ) {
			this.topic = topic;
		}

		public int getYear () {
			return year;
		}

		public void setYear ( final int year ) {
			this.year = year;
		}

		public double getDocuments () {
			return documents;
		}

		public void setDocuments ( final double documents ) {
			this.documents = documents;
		}

		public double getWords () {
			return words;
		}

		public void setWords ( final double words ) {
			this.words = words;
		}

		@Override
		public int compareTo ( final TopicUsageEntry o ) {
			return Integer.compare ( year, o.getYear () );
		}

		@Override
		public String toString () {
			return "TopicUsageEntry{" +
					"topic='" + topic + '\'' +
					", year=" + year +
					", documents=" + documents +
					", words=" + words +
					'}';
		}
	}

	public static final class TopicUsageEntries implements Comparable<TopicUsageEntries> {

		private String topic;

		private List<TopicUsageEntry> entries;

		public TopicUsageEntries ( final String topic, final List<TopicUsageEntry> entries ) {
			this.topic = topic;
			this.entries = entries;
			if ( !entries.isEmpty () ) {
				TopicUsageEntry entry = entries.get ( entries.size () - 1 );
				if ( entry.getWords () < 0 ) {
					entry.setWords ( 0 );
				}
				if ( entry.getDocuments () < 0 ) {
					entry.setDocuments ( 0 );
				}
			}
		}

		public List<TopicUsageEntry> getEntries () {
			return entries;
		}

		public void setEntries ( final List<TopicUsageEntry> entries ) {
			this.entries = entries;
		}

		public String getTopic () {
			return topic;
		}

		public void setTopic ( final String topic ) {
			this.topic = topic;
		}

		@Override
		public int compareTo ( final TopicUsageEntries o ) {
			if ( !entries.isEmpty () && !o.entries.isEmpty () ) {
				double oEntry = o.entries.get ( o.entries.size () - 1 ).getDocuments ();
				double entry = entries.get ( entries.size () - 1 ).getDocuments ();
				return Double.compare ( oEntry, entry );
			}
			return 0;
		}
	}
}
