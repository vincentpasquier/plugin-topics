/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.topics;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.topics.extractor.TopicModelProcessor;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.annotations.ListParameter;
import ch.hesso.websocket.annotations.LongParameter;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TopicPluginInterface extends PluginInterface {

	@ListParameter (value = { },
			description = "Keywords search used to broaden conference analysis.")
	private String[] keywords;

	@ListParameter (value = { },
			description = "Conference acronym used to specify which conference are targeted.")
	private String[] acronyms;

	@ListParameter (value = { },
			description = "Conference titles used to specify which conference are targeted.")
	private String[] titles;

	@LongParameter (value = 2000, description = "The year at which to start gathering abstracts from (inclusive).")
	private long startYear;

	@LongParameter (value = 2014, description = "The year at which to stop gathering abstracts from (inclusive).")
	private long endYear;

	@LongParameter (value = 10, description = "The year at which to stop gathering abstracts from (inclusive).")
	private long topicNumber;

	@CallableMethod (value = "Topic analysis", description = "Performs a series of topic modeling analysis as well as gaussian process regression. To see the result, go to \"Recommendation\".")
	public void performTopicAnalysis () {
		TopicModelProcessor topicModelProcessor = new TopicModelProcessor (
				startYear,
				endYear,
				topicNumber,
				keywords,
				acronyms,
				titles
		);
		try {
			topicModelProcessor.run ();
		} catch ( Exception e ) {
			PluginStatus.Builder builder =
					PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR );
			builder.text ( "An error occurred when running your query to analyze topic trends with given parameters" );
			PluginInterface.publishPluginStatus ( builder.build () );
		}
	}

	@Override
	public String name () {
		return "Topic over time model tool";
	}

	@Override
	public String description () {
		return "Performs topic over time modeling with publications provided. Results are then analyzed with regression methods to find which topic fair the best in the future. For visualisation refer to the \"Recommendation\" menu.";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		return new HashMap<> ();
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return new HashSet<> ();
	}
}
